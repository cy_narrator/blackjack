logo = """
__________ .__                    __             ____.                 __     
\______   \|  |  _____     ____  |  | __        |    |_____     ____  |  | __ 
 |    |  _/|  |  \__  \  _/ ___\ |  |/ /        |    |\__  \  _/ ___\ |  |/ / 
 |    |   \|  |__ / __ \_\  \___ |    <     /\__|    | / __ \_\  \___ |    <  
 |______  /|____/(____  / \___  >|__|_ \    \________|(____  / \___  >|__|_ \ 
        \/            \/      \/      \/                   \/      \/      \/ 
                                                                              
"""


import random
import os
blackjack_list = [10,'J','Q','K']
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
cards = [
    {'A': [1,11]}, 
    {1: 1}, 
    {2: 2}, 
    {3: 3}, 
    {4: 4},
    {5: 5},
    {6: 6}, 
    {7: 7}, 
    {8: 8}, 
    {9: 9}, 
    {10: 10},
    {'J': 10},
    {'Q': 10},
    {'K': 10},
]

def line():
    print('-------------------------------------')

def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')

def sum_total(count_cards):
    sum = 0
    for card in count_cards:
        sum += card
    return sum

player_show_cards = []
player_count_cards = []
computer_count_cards = []
computer_show_cards = []

def display_player_hand(player_show_cards,player_count_cards,first = False):
    if first:
        print("\n")
        print(f"Your first card is {player_show_cards} which is of value {sum_total(player_count_cards)}")
        print("\n")
    else:
        print("\n")
        print(f"Your hand is {player_show_cards} totalling {sum_total(player_count_cards)}")
        print("\n")

def display_computer_hand(computer_show_cards,computer_count_cards,first = True):
    if first:
        print("\n")
        print(f"Computer's first card is {computer_show_cards[0]}, which is of value {sum_total(computer_count_cards)}.")
        print("\n")
    else:
        print("\n")
        print(f"Computer's hand is {computer_show_cards} totaling {sum_total(computer_count_cards)}.")
        print("\n")

def card_chooser(count_cards,show_card):
    return_card = {}
    chosen_card = random.choice(cards)
    chosen_card = list(chosen_card.keys())[0]
    if chosen_card not in numbers:
        if chosen_card == 'A':
            total_hand = sum_total(count_cards)
            ace_list = cards[0]['A']
            if total_hand < 11:
                return_card['A'] = ace_list[1]
            else:
                return_card['A'] = ace_list[0]
        else:
            return_card[chosen_card] = 10
    else:
        return_card[chosen_card] = chosen_card

    count_cards.append(return_card[chosen_card]) #Add the value to the count card
    show_card.append(list(return_card.keys())[0]) #Add the key to the show card

    return return_card
           

def play_game():
    game_is_on = True
    card_chooser(player_count_cards,player_show_cards)
    card_chooser(computer_count_cards,computer_show_cards)
    display_computer_hand(computer_count_cards,computer_count_cards)
    display_player_hand(player_show_cards,player_count_cards,True)
    
    while game_is_on:
        exiter = 0
        next_or_pass = input("Enter 'next' for next card or 'pass' to pass: ").lower()
        while len(computer_count_cards) < len(player_count_cards):
            card_chooser(computer_count_cards,computer_show_cards)

            for i in range(2):
                    first_two_cards_computer.append(player_show_cards[i])
            if 'A' in first_two_cards_computer:
                    for possibility in blackjack_list:
                        if possibility in first_two_cards_computer:
                            line()
                            print(f"Computer got a black jack, your hand is {player_show_cards} so you win...")
                            print("\n")
                            break
            
            if sum_total(computer_count_cards) == 21:
                line()
                print(f"Computer's hand is {computer_show_cards} which is 21 so computer wins")
                print("\n")
                game_is_on = False
                exiter += 1
                break

            if sum_total(computer_count_cards) > 21:
                line()
                print(f"Computer hand is {computer_show_cards} totalling {sum_total(computer_count_cards)} which exceeds 21 so you win")
                print("\n")
                game_is_on = False
                exiter += 1
                break

        if exiter == 0:
            if next_or_pass == 'next':
                card_chooser(player_count_cards,player_show_cards)
                display_player_hand(player_show_cards,player_count_cards)
                
                
                first_two_cards_player = [] #Checking if first two cards is a blackjack
                first_two_cards_computer = []
                for i in range(2):
                    first_two_cards_player.append(player_show_cards[i])

                if 'A' in first_two_cards_player:
                    for possibility in blackjack_list:
                        if possibility in first_two_cards_player:
                            line()
                            print(f"You got a black jack, your hand is {player_show_cards} so you win...")
                            print("\n")
                            break
                
                if sum_total(player_count_cards) == 21:
                    line()
                    print(f"Your hand is {player_show_cards} which is exactly 21, you win")
                    print("\n")
                    break

                if sum_total(player_count_cards) > 21:
                    line()
                    print(f"You lose because your hand is {player_show_cards} totalling {sum_total(player_count_cards)} that exceeds 21")
                    print("\n")
                    break
                
            
            else:
                display_computer_hand(computer_show_cards,computer_count_cards,False)
                display_player_hand(player_show_cards,player_count_cards)
                if sum_total(computer_count_cards) > sum_total(player_count_cards):
                    print("\n")
                    line()
                    print(f"Computer hand is {computer_show_cards} totalling {sum_total(computer_count_cards)} which is higher than your total {sum_total(player_count_cards)} so you lose")
                    print("\n")
                elif sum_total(computer_count_cards) < sum_total(player_count_cards):
                    print("\n")
                    line()
                    print(f"Computer hand is {computer_show_cards} totalling {sum_total(computer_count_cards)} which is lower than your total {sum_total(player_count_cards)} so you win")
                    print("\n")
                else:
                    line()
                    print(f"Computer hand is {computer_show_cards} totalling {sum_total(computer_count_cards)} which is equal to your total {sum_total(player_count_cards)} so its a draw")
                    print("\n")
                game_is_on = False
            
                

      
play = True
while play:
    print("\n")
    to_play = input("Do you want to play BlackJack? Type 'yes': ").lower()
    if to_play == 'yes':
        player_show_cards = []
        player_count_cards = []
        computer_count_cards = []
        computer_show_cards = []
        clear()
        print(logo)
        play_game()
    else:
        print("Goodbye...")
        play = False
        exit(0)
        
        
        